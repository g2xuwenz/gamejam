﻿#pragma strict

var g_damage : int = 50;
var g_distance : float;
var g_maxDistance : float = 1.5;

protected var animator: Animator;

function Start () {
	animator = transform.Find("Arms05").GetComponent(Animator);
}

function Update () {
	if (Input.GetKeyDown(KeyCode.Escape)) {
        Application.LoadLevel("GameOver");  
    }

	if ( Input.GetButtonDown("Fire1") || Input.GetButtonDown("Fire2") ) {
		var hit : RaycastHit;
		
		if ( Input.GetButtonDown("Fire1") )
			animator.SetTrigger("LeftPunchTrigger");
		else
			animator.SetTrigger("RightPunchTrigger");
		
		if ( Physics.Raycast( transform.position, transform.TransformDirection( Vector3.forward ), hit )) {
			g_distance = hit.distance;
			
			if ( g_distance < g_maxDistance ) {
				hit.transform.SendMessage("ApplyDamage", g_damage, SendMessageOptions.DontRequireReceiver);
			}
		}
	}
}